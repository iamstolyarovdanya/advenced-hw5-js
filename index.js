class Card {
    constructor(url1, url2) {
        this.url1 = url1;
        this.url2 = url2;
    }
    createCard() {
        fetch(this.url2).then(res => res.json())
            .then((data) => {
                console.log(data);
                data.forEach((elem) => {
                    let div = document.createElement(`div`);
                    div.classList.add(`wrapper`);
                    let p = document.createElement(`p`);
                    p.classList.add(`title`);
                    let p2 = document.createElement(`p`);
                    p2.classList.add(`card__body`)
                    let btn = document.createElement(`button`);
                    let img = document.createElement(`img`);
                    btn.addEventListener(`click`, (event) => {
                        fetch(`https://ajax.test-danit.com/api/json/posts/${elem.id}`, {
                            method: 'DELETE',
                        })
                            .then(response => {
                                if (response.status === 200) {
                                    console.log(elem.id);
                                    if (event.target == btn) {
                                        console.log(event.target);
                                        event.target.parentElement.remove()
                                    }
                                    else if (event.target == img) {
                                        console.log(event.target);
                                        event.target.parentElement.parentElement.remove()
                                    }
                                }
                            })
                            .catch((error) => {
                                console.error(error);
                            })
                    })
                    img.setAttribute(`src`, `./free-icon-close-6781254.png`);
                    btn.append(img)
                    btn.classList.add(`btn`);
                    document.querySelector(`body`).append(div);
                    div.setAttribute(`data`, elem.userId);
                    p.textContent = elem.title;
                    p2.textContent = elem.body;
                    div.append(p, p2, btn);
                })
                fetch(this.url1).then(res => res.json())
                    .then((data) => {
                        data.forEach((element) => {
                            console.log(element);
                            document.querySelectorAll(`div`).forEach((el) => {
                                if (el.getAttribute(`data`) == element.id) {
                                    let h1 = document.createElement(`h1`);
                                    h1.classList.add(`user__name`)
                                    let name = document.createElement(`p`);
                                    name.classList.add(`name`)
                                    name.textContent = `${element.name}`
                                    h1.textContent = ` ${element.username}`;
                                    let span = document.createElement(`span`);
                                    span.classList.add(`user_email`);
                                    span.textContent = `${element.email}`;
                                    el.prepend(h1, span, name);
                                }
                            })
                        })
                    })
                    .catch((err) => {
                        console.error(err)
                    })
            }).catch((err) => {
                console.error(err);
            })
    }
}
const create = new Card(`https://ajax.test-danit.com/api/json/users`, `https://ajax.test-danit.com/api/json/posts`);
create.createCard()
















